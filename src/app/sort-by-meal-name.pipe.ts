import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByMealName'
})
export class SortByMealNamePipe implements PipeTransform {

  transform(array: Array<Meal>): Array<Meal> {
    array.sort((a: any, b: any) => {
      if ( a.strMeal < b.strMeal ) {
        return -1;
      } else if ( a.strMeal > b.strMeal ) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
