import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import {DataService} from '../service/DataService';
import { Observable } from "rxjs/Observable"


@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  private categoryResponse: CategoryResponse;

  constructor(private http: Http, private router: Router, private data: DataService) { }

  ngOnInit(): void {
    this.http.get('https://www.themealdb.com/api/json/v1/1/categories.php')
      .subscribe(response => {
        this.categoryResponse = response.json();
      });

  }
  goToSearchResults(searchValue: String): void {
    // Fetch meals by category
    this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?c=' + searchValue).toPromise()
      .then(response => {
        const mealResponseSimple = response.json();
        // Fetch meal details
        const meals = this.getMealDetails(mealResponseSimple);
        mealResponseSimple.meals = meals;
        this.data.mealResponse = mealResponseSimple;
        this.router.navigate(['/recipe/results']);
      });
  }
  getMealDetails(mealResponse: MealResponse): any {
    const meals: Array<Meal> = new Array<Meal>();
    const promises = [];
    // Fetch Meal Details by id
    for (const meal of mealResponse.meals) {
      const promise = this.http.get('https://www.themealdb.com/api/json/v1/1/lookup.php?i=' + meal.idMeal).toPromise()
        .then(responseDetails => {
          const mealResponseDetail: MealResponse = responseDetails.json();
          meals.push(mealResponseDetail.meals[0]);
        });
      promises.push(promise);
    }
    Promise.all(promises).then(function(values) {
      mealResponse.meals = meals;
      return meals;
    });
  }
}
