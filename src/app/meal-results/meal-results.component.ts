import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../service/DataService';


@Component({
  templateUrl: './meal-results.component.html',
  styleUrls: ['./meal-results.component.css']
})
export class MealResultsComponent implements OnInit {

  constructor(private data: DataService) {}

  ngOnInit() {}

}
