import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealResultsComponent } from './meal-results.component';

describe('MealResultsComponent', () => {
  let component: MealResultsComponent;
  let fixture: ComponentFixture<MealResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
