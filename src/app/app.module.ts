import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


import { HomeComponent } from './home/home.component';
import { MealResultsComponent } from './meal-results/meal-results.component';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { DataService } from './service/DataService';
import { SortByMealNamePipe } from './sort-by-meal-name.pipe';


const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'recipe/results',
    component: MealResultsComponent,
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MealResultsComponent,
    SearchComponent,
    SortByMealNamePipe,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
