import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import {DataService} from '../service/DataService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  private search: String;

  constructor(private http: Http, private router: Router, private data: DataService) { }

  ngOnInit() {}

  goToSearchResults(): void {
    // Fetch meals by category
    this.http.get('https://www.themealdb.com/api/json/v1/1/search.php?s=' + this.search).toPromise()
      .then(response => {
        const mealResponseSimple = response.json();
        // Fetch meal details
        const meals = this.getMealDetails(mealResponseSimple);
        mealResponseSimple.meals = meals;
        this.data.mealResponse = mealResponseSimple;
        this.router.navigate(['/recipe/results']);
      });
  }
  getMealDetails(mealResponse: MealResponse): any {
    const meals: Array<Meal> = new Array<Meal>();
    const promises = [];
    // Fetch Meal Details by id
    for (const meal of mealResponse.meals) {
      const promise = this.http.get('https://www.themealdb.com/api/json/v1/1/lookup.php?i=' + meal.idMeal).toPromise()
        .then(responseDetails => {
          const mealResponseDetail: MealResponse = responseDetails.json();
          meals.push(mealResponseDetail.meals[0]);
        });
      promises.push(promise);
    }
    Promise.all(promises).then(function(values) {
      mealResponse.meals = meals;
      return meals;
    });
  }
  checkEnterKey(event: any) {
    if(event.keyCode === 13) {
      this.goToSearchResults();
    }
  }
}
