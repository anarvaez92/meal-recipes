# Meal-Recipes

The project is a simple application that uses an API from https://www.themealdb.com/api.php to find meal recipes by category (beef, deserts, chicken, and more) and offers the flexibility to search by keyword(s).  

Screenshots:

![Screenshot](screenshot-home.png)

![Screenshot](screenshot-results.png)


#### How To run:

1.) If you do not have angular-cli installed globally, run `npm  install`

2.) Run `ng serve`. Navigate to `http://localhost:4200/`

#### Technologies:
 * ** Node.js **  
 * ** Angular CLI **  
 
#### Frameworks:
 * ** Bootstrap **  
 * ** Jquery **  
 * ** Angular **  

#### Developer: ** Armando Narvaez **
